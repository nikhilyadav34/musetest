<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey='id_tag';
    protected $fillable = [ 'label', 'created_at', 'updated_at'];

 
}
