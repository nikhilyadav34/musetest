<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Tag;

class TagController extends Controller {

    function getTags() {

        return response()->json(Tag::all());
    }

    function create(Request $request) {
         
        $validator = Validator::make($request->all(), ['label' => 'required|unique:tags|min:4|max:50',]);
        if ($validator->fails()) {
            $response['status_code'] = 'failed';
            $response['message'] = $validator->errors()->first();
            return response()->json($response, 400);
        }

        $tag = new Tag();
        $tag->label = $request->input('label');
        $tag->save();
        $response['status_code'] = 'success';
        $response['id_tag'] = $tag->id_tag;
        $response['message'] = 'Tag has been saved successfully.';
        return response()->json($response, 201);
    }
    
    
    
    function update($id_tag, Request $request) {
         
        $validator = Validator::make($request->all(), ['label' => 'required|min:4|max:50',]);
        if ($validator->fails()) {
            $response['status_code'] = 'failed';
            $response['message'] = $validator->errors()->first();
            return response()->json($response, 400);
        }

        $tag = Tag::find($id_tag);
        $tag->label = $request->input('label');
        $tag->save();
        $response['status_code'] = 'success';
        $response['id_tag'] = $tag->id_tag;
        $response['message'] = 'Tag has been updated successfully.';
        return response()->json($response, 201);
    }
    
    
    function searchTag($term) {
        return response()->json(Tag::where('label', 'like', "$term%")->get());
    }

}
