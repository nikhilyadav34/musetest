<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;

class UserController extends Controller {

    function getUsers() {
//        $degree=  User::getDegree(5,4);
//        print_r($degree);
//        $userRel = User::with(['userRelations' =>
//                    function($query) {
//                        $query->leftJoin('tags', 'user_relations.tag_id', '=', 'tags.id_tag')
//                                ->select('users.*', 'tags.*');
//                    }])
//                ->get();
        return response()->json(User::all());
    }

    function getRelations($id_user) {
       
        $user = User::find($id_user);
        $userRelations = $user->userRelations()
                ->leftJoin('tags', 'user_relations.tag_id', '=', 'tags.id_tag')
                ->select('users.*', 'tags.*')
                ->get();
         
        if (count($userRelations)==0) {
            return response()->json([]);
        }
        
        foreach ($userRelations as $relation) {
            $degree = User::getDegree($id_user, $relation->id_user);
            $relation->degree = ($degree!=null)?$degree:0;
        }
        return response()->json($userRelations);
    }

    function create(Request $request) {

        $validator = Validator::make($request->all(), ['fullname' => 'required|unique:users|min:4|max:100']);
        if ($validator->fails()) {
            $response['status_code'] = 'failed';
            $response['message'] = $validator->errors()->first();
            return response()->json($response, 400);
        }

        $user = new User();
        $user->fullname = $request->input('fullname');
        $user->save();
        $response['status_code'] = 'success';
        $response['id_user'] = $user->id_user;
        $response['message'] = 'Person has been added.';
        return response()->json($response, 201);
    }

    function searchUser($term) {
        return response()->json(User::where('fullname', 'like', "$term%")->get());
    }

    function storeRelation($id_user, Request $request) {

        $rule = ['id_user' => 'required|numeric', 'id_tag' => 'required|numeric'];
        $validator = Validator::make($request->all(), $rule);
        if ($validator->fails()) {
            $response['status_code'] = 'failed';
            $response['message'] = $validator->errors()->first();
            return response()->json($response, 400);
        }
        $id_tag = $request->input('id_tag');
        $user_id = $request->input('id_user');

        $user = User::find($id_user);
        $rel_update = $user->userRelations()->attach($user_id, ['tag_id' => $id_tag]);
        $response['status_code'] = 'success';
        $response['message'] = 'Relation has been updated.';
        return response()->json($response, 201);
    }

}
