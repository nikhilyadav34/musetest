<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'id_user';
    protected $fillable = [
        'fullname',
    ];
    protected $hidden = ['pivot'];

    function userRelations() {
        return $this->belongsToMany('App\User', 'user_relations', 'user_one', 'user_two');
    }

    function userTag() {
        return $this->belongsToMany('App\User', 'user_relations', 'user_one', 'tag_id');
    }

     
    /**
     * Degree Sepration mysql query between two people is 1
     * Concept : Taken from 'Frigyes Karinthy' Six degrees of separation theory 
     * Link :https://en.wikipedia.org/wiki/Six_degrees_of_separation
     * 1) First Degree of sepration friend of friend (query 1)
     * 2) Second Degree of sepration friend of friend of friend  (query 2)
     * 3) Third degree, fourth degree so on upto  4 level degree
     * becuase 5-6 degree will start and end person
     * So we are calcuation 6 level degree of sepration in 2 people 
     * 7) Calculation
     * @param int $friend_one
     * @param int $friend_two
     * @return type
     * 
     * otimising db connection using union
     */
    protected function getDegree($friend_one, $friend_two) {


//        SELECT COUNT(*) as count FROM friends f1 WHERE f1.member_id = 1 AND f1.friend_id = 7
//        SELECT COUNT(*) as count
//FROM friends f1
//JOIN friends f2
//  ON f2.member_id = f1.friend_id
//WHERE f1.member_id = 1
//  AND f2.friend_id = 7


        $result1 = \DB::table('user_relations as f1')
                ->where('f1.user_one', '=', $friend_one)
                ->where('f1.user_two', '=', $friend_two)
                ->selectRaw('COUNT(*) as count');
//                ->COUNT('user_one');

        $result2 = \DB::table('user_relations as f1')
                ->leftJoin('user_relations as f2', 'f2.user_one', '=', 'f1.user_two')
                ->where('f1.user_one', '=', $friend_one)
                ->where('f2.user_two', '=', $friend_two)
//                ->COUNT('user_one');
                ->selectRaw('COUNT(*) as count');
        
        $result3 = \DB::table('user_relations as f1')
                ->leftJoin('user_relations as f2', 'f2.user_one', '=', 'f1.user_two')
                ->leftJoin('user_relations as f3', 'f3.user_one', '=', 'f2.user_two')
                ->where('f1.user_one', '=', $friend_one)
                ->where('f3.user_two', '=', $friend_two)
                ->selectRaw('COUNT(*) as count');
//                ->COUNT('user_one');

        $result4 = \DB::table('user_relations as f1')
                ->leftJoin('user_relations as f2', 'f2.user_one', '=', 'f1.user_two')
                ->leftJoin('user_relations as f3', 'f3.user_one', '=', 'f2.user_two')
                ->leftJoin('user_relations as f4', 'f4.user_one', '=', 'f3.user_two')
                ->where('f1.user_one', '=', $friend_one)
                ->where('f4.user_two', '=', $friend_two)
                ->selectRaw('COUNT(*) as count');
//                ->COUNT('user_one');



       

        $results =$result1->union($result2)
                ->union($result3)
                ->union($result4)
                ->get();
        $degree=[];
        foreach ($results as $result) {
            $degree[]=empty($result->count)?0:$result->count;
        }
        rsort($degree);
        return !empty($degree[0])?$degree[0]:0;
    }

}
