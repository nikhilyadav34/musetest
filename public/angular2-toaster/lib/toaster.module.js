"use strict";
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var toast_component_1 = require('./toast.component');
var toaster_container_component_1 = require('./toaster-container.component');
var toaster_service_1 = require('./toaster.service');
var ToasterModule = (function () {
    function ToasterModule() {
    }
    ToasterModule.decorators = [
        { type: core_1.NgModule, args: [{
                    imports: [common_1.CommonModule],
                    declarations: [toast_component_1.ToastComponent, toaster_container_component_1.ToasterContainerComponent],
                    providers: [toaster_service_1.ToasterService],
                    exports: [toaster_container_component_1.ToasterContainerComponent, toast_component_1.ToastComponent],
                },] },
    ];
    /** @nocollapse */
    ToasterModule.ctorParameters = [];
    return ToasterModule;
}());
exports.ToasterModule = ToasterModule;
//# sourceMappingURL=toaster.module.js.map