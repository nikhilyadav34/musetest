export declare class TooltipOptions {
    placement: string;
    popupClass: string;
    animation: boolean;
    appendToBody: boolean;
    isOpen: boolean;
    content: string;
    htmlContent: any;
    context: any;
    constructor(options: Object);
}
