export declare const MAX_CHARS: number;
export declare const MIN_SEARCH_LENGTH: number;
export declare const PAUSE: number;
export declare const TEXT_SEARCHING: string;
export declare const TEXT_NORESULTS: string;
