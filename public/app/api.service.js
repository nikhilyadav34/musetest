System.register(['@angular/core', '@angular/http', 'angular2-toaster/angular2-toaster', './rxjs-operators', 'rxjs/add/operator/map', 'rxjs/add/observable/of', 'rxjs/Observable'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_1, angular2_toaster_1, Observable_1;
    var UserRelations, User, Tag, ApiService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (angular2_toaster_1_1) {
                angular2_toaster_1 = angular2_toaster_1_1;
            },
            function (_1) {},
            function (_2) {},
            function (_3) {},
            function (Observable_1_1) {
                Observable_1 = Observable_1_1;
            }],
        execute: function() {
            UserRelations = (function () {
                function UserRelations(id_tag, label, id_user, fullname, status, created_at, updated_at) {
                    this.id_tag = id_tag;
                    this.label = label;
                    this.id_user = id_user;
                    this.fullname = fullname;
                    this.status = status;
                    this.created_at = created_at;
                    this.updated_at = updated_at;
                }
                return UserRelations;
            }());
            exports_1("UserRelations", UserRelations);
            User = (function () {
                function User(id_user, fullname, status, created_at, updated_at, User_relations) {
                    this.id_user = id_user;
                    this.fullname = fullname;
                    this.status = status;
                    this.created_at = created_at;
                    this.updated_at = updated_at;
                    this.User_relations = User_relations;
                }
                return User;
            }());
            exports_1("User", User);
            Tag = (function () {
                function Tag(id_tag, label) {
                    this.id_tag = id_tag;
                    this.label = label;
                }
                return Tag;
            }());
            exports_1("Tag", Tag);
            ApiService = (function () {
                function ApiService(http, toasterService) {
                    this.http = http;
                    this.toasterService = toasterService;
                    if (document.location.hostname == 'localhost') {
                        this.baseApi = '/webapi/';
                    }
                    else {
                        this.baseApi = '/webapi/';
                    }
                    this.toasterService = toasterService;
                }
                ApiService.prototype.getUsers = function () {
                    return this.http.get(this.baseApi + 'user')
                        .toPromise()
                        .then(this.extractData)
                        .catch(this.handleError);
                };
                ApiService.prototype.getTags = function () {
                    return this.http.get(this.baseApi + 'tag')
                        .toPromise()
                        .then(this.extractData)
                        .catch(this.handleError);
                };
                ApiService.prototype.extractData = function (res) {
                    var body = res.json();
                    //console.log(body)
                    return body || {};
                };
                ApiService.prototype.handleError = function (err) {
                    try {
                        var error = err.json();
                        if (error.status_code == 'failed') {
                            this.toasterService.pop('error', 'Error!', error.message);
                        }
                        else {
                            this.toasterService.pop('error', 'Error!', 'An error occured. Please try again later.');
                        }
                        console.log(error);
                    }
                    catch (e) {
                    }
                    return Promise.reject("");
                };
                ApiService.prototype.addTag = function (tag) {
                    var _this = this;
                    return this.http.post(this.baseApi + 'tag', tag)
                        .toPromise()
                        .then(function (res) { return res.json(); })
                        .catch(function (error) { return _this.handleError(error); });
                };
                ApiService.prototype.editTag = function (tag) {
                    var _this = this;
                    return this.http.put(this.baseApi + ("tag/" + tag.id_tag), tag)
                        .toPromise()
                        .then(function (res) { return res.json(); })
                        .catch(function (error) { return _this.handleError(error); });
                };
                ApiService.prototype.addUser = function (user) {
                    var _this = this;
                    return this.http.post(this.baseApi + 'user', user)
                        .toPromise()
                        .then(function (res) { return res.json(); })
                        .catch(function (error) { return _this.handleError(error); });
                };
                //  searchTag(tagLabel:string) {
                //     return this.http.get(this.baseApi + 'tag/${tagLabel}/search')
                //         .toPromise()
                //         .then(res => res.json())
                //         .catch(this.handleError);
                // }
                ApiService.prototype.filterPerson = function (res, excludeKeys) {
                    var res = res.json();
                    var filteredUser = [];
                    res.forEach(function (usrObj) {
                        if (excludeKeys.indexOf(usrObj.id_user) == -1) {
                            filteredUser.push(usrObj);
                        }
                    });
                    return filteredUser;
                };
                ApiService.prototype.searchPerson = function (fullName, excludeKeys) {
                    var _this = this;
                    if (fullName === '') {
                        return Observable_1.Observable.of([]);
                    }
                    return this.http.get(this.baseApi + ("user/" + fullName + "/search"))
                        .map(function (res) { return _this.filterPerson(res, excludeKeys); })
                        .catch(this.handleError);
                };
                ApiService.prototype.searchTag = function (label) {
                    if (label === '') {
                        return Observable_1.Observable.of([]);
                    }
                    return this.http.get(this.baseApi + ("tag/" + label + "/search"))
                        .map(function (res) { return res.json(); })
                        .catch(this.handleError);
                };
                ApiService.prototype.saveRelation = function (id_user, userRel) {
                    var _this = this;
                    return this.http.post(this.baseApi + ("user/" + id_user + "/relation"), userRel)
                        .toPromise()
                        .then(function (res) { return res.json(); })
                        .catch(function (error) { return _this.handleError(error); });
                };
                ApiService.prototype.getRelation = function (id_user) {
                    var _this = this;
                    return this.http.get(this.baseApi + ("user/" + id_user + "/relation"))
                        .toPromise()
                        .then(function (res) { return res.json(); })
                        .catch(function (error) { return _this.handleError(error); });
                };
                ApiService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http, angular2_toaster_1.ToasterService])
                ], ApiService);
                return ApiService;
            }());
            exports_1("ApiService", ApiService);
        }
    }
});

//# sourceMappingURL=api.service.js.map
