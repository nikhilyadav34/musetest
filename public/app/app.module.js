System.register(['@angular/core', '@angular/platform-browser', '@angular/router', '@angular/http', '@ng-bootstrap/ng-bootstrap', '@angular/forms', 'ng2-modal', 'angular2-toaster/angular2-toaster', './app.component', './users.component', './add-user.component', './add-tag.component', './tags.component', './users-resolve.service', './tags-resolve.service', './api.service', './user-relation.component', './edit-tag.component', './orderBy'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, platform_browser_1, router_1, http_1, ng_bootstrap_1, forms_1, ng2_modal_1, angular2_toaster_1, app_component_1, users_component_1, add_user_component_1, add_tag_component_1, tags_component_1, users_resolve_service_1, tags_resolve_service_1, api_service_1, user_relation_component_1, edit_tag_component_1, orderBy_1;
    var AppModule;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (platform_browser_1_1) {
                platform_browser_1 = platform_browser_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (ng_bootstrap_1_1) {
                ng_bootstrap_1 = ng_bootstrap_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
            },
            function (ng2_modal_1_1) {
                ng2_modal_1 = ng2_modal_1_1;
            },
            function (angular2_toaster_1_1) {
                angular2_toaster_1 = angular2_toaster_1_1;
            },
            function (app_component_1_1) {
                app_component_1 = app_component_1_1;
            },
            function (users_component_1_1) {
                users_component_1 = users_component_1_1;
            },
            function (add_user_component_1_1) {
                add_user_component_1 = add_user_component_1_1;
            },
            function (add_tag_component_1_1) {
                add_tag_component_1 = add_tag_component_1_1;
            },
            function (tags_component_1_1) {
                tags_component_1 = tags_component_1_1;
            },
            function (users_resolve_service_1_1) {
                users_resolve_service_1 = users_resolve_service_1_1;
            },
            function (tags_resolve_service_1_1) {
                tags_resolve_service_1 = tags_resolve_service_1_1;
            },
            function (api_service_1_1) {
                api_service_1 = api_service_1_1;
            },
            function (user_relation_component_1_1) {
                user_relation_component_1 = user_relation_component_1_1;
            },
            function (edit_tag_component_1_1) {
                edit_tag_component_1 = edit_tag_component_1_1;
            },
            function (orderBy_1_1) {
                orderBy_1 = orderBy_1_1;
            }],
        execute: function() {
            AppModule = (function () {
                function AppModule() {
                }
                AppModule = __decorate([
                    core_1.NgModule({
                        imports: [
                            platform_browser_1.BrowserModule, forms_1.FormsModule, http_1.HttpModule, http_1.JsonpModule, ng_bootstrap_1.NgbModule.forRoot(), angular2_toaster_1.ToasterModule, ng2_modal_1.ModalModule,
                            router_1.RouterModule.forRoot([
                                { path: '', component: users_component_1.UsersComponent, resolve: { users: users_resolve_service_1.UsersListResolve } },
                                { path: 'tags', component: tags_component_1.TagsComponent, resolve: { tags: tags_resolve_service_1.TagsListResolve } },
                            ])
                        ],
                        declarations: [app_component_1.AppComponent, users_component_1.UsersComponent, tags_component_1.TagsComponent, add_user_component_1.AddUserComponent, add_tag_component_1.AddTagComponent,
                            user_relation_component_1.UserRelationComponent, user_relation_component_1.UserRelationComponent, edit_tag_component_1.EditTagComponent, orderBy_1.OrderBy],
                        bootstrap: [app_component_1.AppComponent],
                        entryComponents: [edit_tag_component_1.EditTagComponent],
                        providers: [users_resolve_service_1.UsersListResolve, tags_resolve_service_1.TagsListResolve, api_service_1.ApiService]
                    }), 
                    __metadata('design:paramtypes', [])
                ], AppModule);
                return AppModule;
            }());
            exports_1("AppModule", AppModule);
        }
    }
});

//# sourceMappingURL=app.module.js.map
