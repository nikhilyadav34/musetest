System.register(['@angular/core', '@ng-bootstrap/ng-bootstrap', './api.service', 'angular2-toaster/angular2-toaster', './rxjs-operators', 'rxjs/add/operator/map'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, ng_bootstrap_1, api_service_1, angular2_toaster_1;
    var EditTagComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ng_bootstrap_1_1) {
                ng_bootstrap_1 = ng_bootstrap_1_1;
            },
            function (api_service_1_1) {
                api_service_1 = api_service_1_1;
            },
            function (angular2_toaster_1_1) {
                angular2_toaster_1 = angular2_toaster_1_1;
            },
            function (_1) {},
            function (_2) {}],
        execute: function() {
            EditTagComponent = (function () {
                function EditTagComponent(activeModal, toasterService, apiService) {
                    this.activeModal = activeModal;
                    this.toasterService = toasterService;
                    this.apiService = apiService;
                    this.submitted = false;
                    this.model = new api_service_1.Tag();
                }
                EditTagComponent.prototype.ngOnInit = function () {
                    this.model = Object.assign({}, this.tag);
                };
                EditTagComponent.prototype.submitHandler = function (res) {
                    if (res.status_code == 'success') {
                        this.activeModal.close(this.model);
                        this.toasterService.pop('success', 'Success!', 'Relationship tag has been added.');
                        this.model.id_tag = res.id_tag;
                    }
                    else {
                        this.toasterService.pop('error', 'Error!', 'An error occured. Please try again later.');
                    }
                    this.submitted = false;
                };
                EditTagComponent.prototype.handleError = function (error) {
                    console.log(error);
                };
                EditTagComponent.prototype.onSubmit = function () {
                    var _this = this;
                    this.submitted = true;
                    this.apiService.editTag(this.model)
                        .then(function (res) { return _this.submitHandler(res); })
                        .catch(function (error) { return _this.handleError(error); });
                };
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Object)
                ], EditTagComponent.prototype, "tag", void 0);
                EditTagComponent = __decorate([
                    core_1.Component({
                        selector: 'edit-tag',
                        template: "  <div class=\"modal-header\">\n      <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"activeModal.dismiss('Cross click')\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n      <h4 class=\"modal-title\">Edit a relationship tag</h4>\n    </div>\n    <div class=\"modal-body\">\n      \n      \n      <form  #editTagForm=\"ngForm\" novalidate (ngSubmit)=\"onSubmit()\"  >\n \n  <div class=\"modal-body\">\n\n<div class=\"input-group\">\n  <label for=\"basic-url\">Relationship Tag Name</label>\n \n  \n  <input type=\"text\" class=\"form-control\"aria-describedby=\"basic-addon3\" placeholder=\"Relationship Tag Name\"\n        id=\"label\" \n       required minlength=\"4\" maxlength=\"40\"\n       [(ngModel)]=\"model.label\" name=\"label\" #label=\"ngModel\">\n       \n       <div *ngIf=\"label.errors && (label.dirty || label.touched)\"\n     class=\"alert alert-danger\">\n    <div [hidden]=\"!label.errors.required\">\n      Tag is required\n    </div>\n    <div [hidden]=\"!label.errors.minlength\">\n      Tag must be at least 4 characters long.\n    </div>\n    <div [hidden]=\"!label.errors.maxlength\">\n      Tag cannot be more than 40 characters long.\n    </div>\n</div>\n</div>\n    \n  </div>  \n \n  <div class=\"modal-footer\">\n   <div *ngIf=\"submitted\">Please wait...</div>\n     <input type=\"submit\" class=\"btn btn-primary btn-group-lg\" value=\"Save\" [disabled]=\"!editTagForm.form.valid\" >\n    <button type=\"button\" class=\"btn btn-secondary\" (click)=\"activeModal.dismiss('Close click')\">Close</button>\n  </div>\n  </form>\n "
                    }), 
                    __metadata('design:paramtypes', [ng_bootstrap_1.NgbActiveModal, angular2_toaster_1.ToasterService, api_service_1.ApiService])
                ], EditTagComponent);
                return EditTagComponent;
            }());
            exports_1("EditTagComponent", EditTagComponent);
        }
    }
});

//# sourceMappingURL=edit-tag.component.js.map
