System.register(['@angular/core', './api.service', 'angular2-toaster/angular2-toaster', './rxjs-operators', 'rxjs/add/operator/map', 'rxjs/add/observable/of', 'rxjs/Observable'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, api_service_1, angular2_toaster_1, Observable_1;
    var UserRelationComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (api_service_1_1) {
                api_service_1 = api_service_1_1;
            },
            function (angular2_toaster_1_1) {
                angular2_toaster_1 = angular2_toaster_1_1;
            },
            function (_1) {},
            function (_2) {},
            function (_3) {},
            function (Observable_1_1) {
                Observable_1 = Observable_1_1;
            }],
        execute: function() {
            UserRelationComponent = (function () {
                function UserRelationComponent(apiService, toasterService) {
                    var _this = this;
                    this.apiService = apiService;
                    this.toasterService = toasterService;
                    this.searching = false;
                    this.searchFailed = false;
                    this.active = true;
                    this.loadingRelations = false;
                    this.searchPerson = function (text$) {
                        return text$
                            .debounceTime(300)
                            .distinctUntilChanged()
                            .do(function () { return _this.searching = true; })
                            .switchMap(function (term) {
                            return _this.apiService.searchPerson(term, _this.excludeKeys)
                                .do(function () { return _this.searchFailed = false; })
                                .catch(_this.handleError);
                        })
                            .do(function () { return _this.searching = false; });
                    };
                    this.formatter = function (x) { return x.fullname; };
                    this.searchTag = function (text$) {
                        return text$
                            .debounceTime(300)
                            .distinctUntilChanged()
                            .do(function () { return _this.searching = true; })
                            .switchMap(function (term) {
                            return _this.apiService.searchTag(term)
                                .do(function () { return _this.searchFailed = false; })
                                .catch(_this.handleError);
                        })
                            .do(function () { return _this.searching = false; });
                    };
                    this.formatterTag = function (xTag) { return xTag.label; };
                    this.model = new api_service_1.UserRelations();
                }
                UserRelationComponent.prototype.handleError = function (error) {
                    console.log(error);
                    return Observable_1.Observable.of([]);
                };
                UserRelationComponent.prototype.open = function (currentUser) {
                    var _this = this;
                    this.loadingRelations = true;
                    this.currentUser = currentUser;
                    this.fullname = currentUser.fullname;
                    this.apiService.getRelation(currentUser.id_user)
                        .then(function (userRelations) {
                        _this.userRelations = userRelations;
                        _this.loadingRelations = false;
                        _this.getExcludeKeys();
                    })
                        .catch(function (error) {
                        _this.toasterService.pop('error', 'Error!', 'An error occured. Please try again later.');
                        _this.loadingRelations = false;
                    });
                    // this.userRelations = currentUser.user_relations;
                    this.myModal.open();
                    this.model = new api_service_1.UserRelations();
                };
                UserRelationComponent.prototype.typeHeadPersonChange = function (eventData) {
                    this.model.fullname = '';
                };
                UserRelationComponent.prototype.typeHeadTagChange = function (eventData) {
                    this.model.label = '';
                };
                UserRelationComponent.prototype.actionOnOpen = function () {
                    this.model = new api_service_1.UserRelations();
                };
                UserRelationComponent.prototype.onRelatatedSelect = function (event) {
                    this.currentRelatatedUser = event.item;
                };
                UserRelationComponent.prototype.getExcludeKeys = function () {
                    var excludeKeys = [];
                    this.userRelations.forEach(function (usrObj) {
                        excludeKeys.push(usrObj.id_user);
                    });
                    this.excludeKeys = excludeKeys;
                    this.excludeKeys.push(this.currentUser.id_user);
                    // console.log(this.excludeKeys)
                };
                UserRelationComponent.prototype.onRelationSelect = function (event) {
                    this.currentRelationTag = event.item;
                };
                UserRelationComponent.prototype.onRelationAdd = function (res, user_relations) {
                    var _this = this;
                    // console.log(this.currentUser)
                    if (res.status_code == 'success') {
                        this.userRelations = [];
                        this.apiService.getRelation(this.currentUser.id_user)
                            .then(function (userRelations) {
                            _this.userRelations = userRelations;
                            _this.loadingRelations = false;
                            _this.getExcludeKeys();
                        })
                            .catch(function (error) {
                            _this.loadingRelations = false;
                        });
                        this.model = new api_service_1.UserRelations();
                        this.active = false;
                        this.getExcludeKeys();
                        setTimeout(function () { return _this.active = true; }, 0);
                        this.toasterService.pop('success', 'Success!', 'Relationship has been updated.');
                    }
                    else {
                        this.toasterService.pop('error', 'Error!', 'An error occured. Please try again later.');
                    }
                };
                UserRelationComponent.prototype.onRelationError = function (err) {
                    // this.toasterService.pop('error', 'Error!', 'An error occured. Please try again later.')
                    console.log(err);
                };
                UserRelationComponent.prototype.addRelation = function () {
                    var _this = this;
                    var user_relations = this.currentRelatatedUser;
                    user_relations.id_tag = this.currentRelationTag.id_tag;
                    user_relations.label = this.currentRelationTag.label;
                    this.apiService.saveRelation(this.currentUser.id_user, user_relations)
                        .then(function (res) { return _this.onRelationAdd(res, user_relations); })
                        .catch(function (err) { return _this.onRelationError(err); });
                };
                __decorate([
                    core_1.ViewChild('myModal'), 
                    __metadata('design:type', Object)
                ], UserRelationComponent.prototype, "myModal", void 0);
                UserRelationComponent = __decorate([
                    core_1.Component({
                        selector: 'user-relation',
                        template: "<div class=\"row\">\n <modal #myModal  (onOpen)=\"actionOnOpen()\" modalClass=\"modal-lg\">\n        <modal-header>\n            <h3>Add the person's relationships</h3>\n        </modal-header>\n        <modal-content>\n        \n      <div class=\"col-xs-6 col-sm-4\">    \n  <div class=\"row\">\n   <h3>{{fullname}}</h3>\n  <p>Relationship</p>\n</div>\n</div>\n   \n<div class=\"inline-break\"></div>\n<div class=\"clearfix visible-xs-block\"></div>\n \n<template #rt let-r=\"result\" let-t=\"term\">\n              {{ r.fullname}}\n            </template>\n            \n \n<template #rtTag let-r=\"result\" let-t=\"term\"  >\n              {{ r.label}}\n            </template>           \n            \n <form  #userRelationForm=\"ngForm\" novalidate (ngSubmit)=\"addRelation()\"   *ngIf=\"active\">\n<div class=\"row\">\n  <div class=\"col-xs-6 col-sm-4\">\n  <input type=\"text\" name=\"relatatedPerson\" class=\"form-control\" aria-label=\"Name-Relatated Person\"  \n  (change)=\"typeHeadPersonChange($event)\" [focusFirst]=\"true\" [editable]=\"false\" \n  required placeholder=\"Name-Relatated Person\" #relatatedPerson=\"ngModel\"\n  [(ngModel)]=\"model.fullname\" \n   [ngbTypeahead]=\"searchPerson\"  [inputFormatter]=\"formatter\" [resultTemplate]=\"rt\"\n    (selectItem)=\"onRelatatedSelect($event)\">\n    <div [hidden]=\"relatatedPerson.valid || relatatedPerson.pristine\" class=\"alert alert-danger \"> \n        Name-Relatated Person is required.\n    </div>\n       <span *ngIf=\"searching\">searching...</span>\n\n  </div>\n  <div class=\"col-xs-6 col-sm-4\">\n  \n   <input type=\"text\" name=\"relatatedTag\" class=\"form-control\" aria-label=\"Relation Tag\" \n   (change)=\"typeHeadTagChange($event)\" [focusFirst]=\"true\" [editable]=\"false\" \n   required placeholder=\"Relation Tag\" #relatatedTag=\"ngModel\"\n  [(ngModel)]=\"model.label\" \n   [ngbTypeahead]=\"searchTag\"  [inputFormatter]=\"formatterTag\" [resultTemplate]=\"rtTag\"\n    (selectItem)=\"onRelationSelect($event)\">\n    <div [hidden]=\"relatatedTag.valid || relatatedTag.pristine\" class=\"alert alert-danger \"> \n        Relation Tag is required.\n    </div>\n   <span *ngIf=\"searching\">searching...</span>\n  \n  \n \n  <div class=\"inline-break\"></div>\n  <div class=\"clearfix visible-xs-block\"></div>\n</div>\n  <div class=\"clearfix visible-xs-block\"></div>\n  <div class=\"col-xs-6 col-sm-4\">\n  <input type=\"submit\" class=\"btn btn-primary\" value=\"Add\" [disabled]=\"!userRelationForm.form.valid\" >\n</div>\n</div> \n       </form> \n       \n      <div class=\"inline-break\"></div>\n  <div class=\"clearfix visible-xs-block\"></div>  \n  \n  \n  <div *ngIf=\"loadingRelations\">Loading Relations...</div>\n   <table class=\"table table-bordered\">\n    <tr *ngFor=\"let userRelation of userRelations\">\n        <td>{{fullname}}</td>\n        <td>{{userRelation.label}}</td>\n        <td>{{userRelation.fullname}}</td>\n        <td>{{userRelation.degree}}</td>\n         \n    </tr>\n</table>     \n          \n          \n          \n        </modal-content>\n        <modal-footer>\n            <button class=\"btn btn-primary\" (click)=\"myModal.close()\">close</button>\n        </modal-footer>\n    </modal>\n</div>\n"
                    }), 
                    __metadata('design:paramtypes', [api_service_1.ApiService, angular2_toaster_1.ToasterService])
                ], UserRelationComponent);
                return UserRelationComponent;
            }());
            exports_1("UserRelationComponent", UserRelationComponent);
        }
    }
});

//# sourceMappingURL=user-relation.component.js.map
