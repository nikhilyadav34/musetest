System.register(['@angular/core', '@angular/router', './api.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, api_service_1;
    var TagsListResolve;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (api_service_1_1) {
                api_service_1 = api_service_1_1;
            }],
        execute: function() {
            TagsListResolve = (function () {
                function TagsListResolve(apiService, router) {
                    this.apiService = apiService;
                    this.router = router;
                }
                TagsListResolve.prototype.resolve = function (route) {
                    var _this = this;
                    return this.apiService.getTags()
                        .then(function (tags) {
                        console.log(tags);
                        if (tags) {
                            return tags;
                        }
                        else {
                            _this.router.navigate(['/']);
                            return false;
                        }
                    });
                };
                TagsListResolve = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [api_service_1.ApiService, router_1.Router])
                ], TagsListResolve);
                return TagsListResolve;
            }());
            exports_1("TagsListResolve", TagsListResolve);
        }
    }
});

//# sourceMappingURL=tags-resolve.service.js.map
