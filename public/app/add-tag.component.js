System.register(['@angular/core', '@angular/router', '@ng-bootstrap/ng-bootstrap', './api.service', 'angular2-toaster/angular2-toaster', './rxjs-operators', 'rxjs/add/operator/map'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, ng_bootstrap_1, api_service_1, angular2_toaster_1;
    var AddTagComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (ng_bootstrap_1_1) {
                ng_bootstrap_1 = ng_bootstrap_1_1;
            },
            function (api_service_1_1) {
                api_service_1 = api_service_1_1;
            },
            function (angular2_toaster_1_1) {
                angular2_toaster_1 = angular2_toaster_1_1;
            },
            function (_1) {},
            function (_2) {}],
        execute: function() {
            AddTagComponent = (function () {
                function AddTagComponent(modalService, router, apiService, toasterService) {
                    this.modalService = modalService;
                    this.router = router;
                    this.apiService = apiService;
                    this.toasterService = toasterService;
                    this.updateTagsEvent = new core_1.EventEmitter();
                    this.submitted = false;
                    this.searching = false;
                    this.searchFailed = false;
                }
                AddTagComponent.prototype.open = function (content) {
                    this.modalRef = this.modalService.open(content);
                    this.model = new api_service_1.Tag();
                    this.router.navigate(['tags']);
                };
                AddTagComponent.prototype.getDismissReason = function (reason) {
                    if (reason === ng_bootstrap_1.ModalDismissReasons.ESC) {
                        return 'by pressing ESC';
                    }
                    else if (reason === ng_bootstrap_1.ModalDismissReasons.BACKDROP_CLICK) {
                        return 'by clicking on a backdrop';
                    }
                    else {
                        return "with: " + reason;
                    }
                };
                AddTagComponent.prototype.submitHandler = function (res) {
                    if (res.status_code == 'success') {
                        this.modalRef.close();
                        this.toasterService.pop('success', 'Success!', 'Relationship tag has been added.');
                        this.model.id_tag = res.id_tag;
                        this.updateTagsEvent.emit(this.model);
                    }
                    else {
                        this.toasterService.pop('error', 'Error!', 'An error occured. Please try again later.');
                    }
                    this.submitted = false;
                };
                AddTagComponent.prototype.onSubmit = function () {
                    var _this = this;
                    this.submitted = true;
                    this.apiService.addTag(this.model)
                        .then(function (res) { return _this.submitHandler(res); });
                };
                __decorate([
                    core_1.Output(), 
                    __metadata('design:type', Object)
                ], AddTagComponent.prototype, "updateTagsEvent", void 0);
                AddTagComponent = __decorate([
                    core_1.Component({
                        selector: 'add-tag',
                        template: "\n            <button type=\"button\" class=\"btn btn-primary btn-group-lg\" (click)=\"open(content)\">Add Tag</button>\n\n<template #content let-c=\"close\" let-d=\"dismiss\">\n <form  #addTagForm=\"ngForm\" novalidate (ngSubmit)=\"onSubmit()\"  >\n  <div class=\"modal-header\">\n    <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n    <h4 class=\"modal-title\">Add a new relationship tag name</h4>\n  </div>\n  <div class=\"modal-body\">\n\n<div class=\"input-group\">\n  <label for=\"basic-url\">Relationship Tag Name</label>\n \n  \n  <input type=\"text\" class=\"form-control\"aria-describedby=\"basic-addon3\" placeholder=\"Relationship Tag Name\"\n        id=\"label\" \n       required minlength=\"4\" maxlength=\"40\"\n       [(ngModel)]=\"model.label\" name=\"label\" #label=\"ngModel\">\n       \n       <div *ngIf=\"label.errors && (label.dirty || label.touched)\"\n     class=\"alert alert-danger\">\n    <div [hidden]=\"!label.errors.required\">\n      Tag is required\n    </div>\n    <div [hidden]=\"!label.errors.minlength\">\n      Tag must be at least 4 characters long.\n    </div>\n    <div [hidden]=\"!label.errors.maxlength\">\n      Tag cannot be more than 40 characters long.\n    </div>\n</div>\n</div>\n    \n  </div>  \n \n  <div class=\"modal-footer\">\n   <div *ngIf=\"submitted\">Please wait...</div>\n     <input type=\"submit\" class=\"btn btn-primary btn-group-lg\" value=\"Save\" [disabled]=\"!addTagForm.form.valid\" >\n    <button type=\"button\" class=\"btn btn-secondary\" (click)=\"c('Close click')\">Close</button>\n  </div>\n  </form>\n</template>\n \n "
                    }), 
                    __metadata('design:paramtypes', [ng_bootstrap_1.NgbModal, router_1.Router, api_service_1.ApiService, angular2_toaster_1.ToasterService])
                ], AddTagComponent);
                return AddTagComponent;
            }());
            exports_1("AddTagComponent", AddTagComponent);
        }
    }
});

//# sourceMappingURL=add-tag.component.js.map
