System.register(['@angular/core', '@angular/router', '@ng-bootstrap/ng-bootstrap', './user-relation.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, ng_bootstrap_1, user_relation_component_1;
    var UsersComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (ng_bootstrap_1_1) {
                ng_bootstrap_1 = ng_bootstrap_1_1;
            },
            function (user_relation_component_1_1) {
                user_relation_component_1 = user_relation_component_1_1;
            }],
        execute: function() {
            // import {OrderByPipe} from './orderBy'
            UsersComponent = (function () {
                function UsersComponent(route, router, modalService) {
                    this.route = route;
                    this.router = router;
                    this.modalService = modalService;
                }
                UsersComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.route.data.forEach(function (data) {
                        console.log(data);
                        _this.users = data.users;
                    });
                };
                UsersComponent.prototype.updateUser = function (user) {
                    this.users.push(user);
                };
                UsersComponent.prototype.open = function (user) {
                    this.userRelComp.open(user);
                };
                __decorate([
                    core_1.ViewChild(user_relation_component_1.UserRelationComponent), 
                    __metadata('design:type', user_relation_component_1.UserRelationComponent)
                ], UsersComponent.prototype, "userRelComp", void 0);
                UsersComponent = __decorate([
                    core_1.Component({
                        // pipes: [ OrderByPipe ],
                        template: "\n \n<div class=\"inline-break\"></div>\n \n    <div class=\"row\">\n        <div class=\"col-xs-6 col-lg-6 text-center\">\n            <add-user (updateUsersEvent)=\"updateUser($event)\"></add-user>\n        </div> \n        <div class=\"col-xs-6 col-lg-6 text-center\">\n            <add-tag (updateTagsEvent)=\"updateTags($event)\"></add-tag>\n        </div>                \n    </div>\n<user-relation #myModal></user-relation>\n\n  <div class=\"inline-break\"></div>\n  <div class=\"clearfix visible-xs-block\"></div>  \n\n <table class=\"table table-bordered\">\n    <tr *ngFor=\"let user of users |  orderBy : ['-id_user']\">\n        <td >{{user.fullname}}</td>\n        <td align=\"right\"> \n        <button class=\"btn btn-primary\" (click)=\"open(user)\">Edit</button>\n            </td>\n    </tr>\n</table>"
                    }), 
                    __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, ng_bootstrap_1.NgbModal])
                ], UsersComponent);
                return UsersComponent;
            }());
            exports_1("UsersComponent", UsersComponent);
        }
    }
});

//# sourceMappingURL=users.component.js.map
