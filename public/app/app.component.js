System.register(['@angular/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent() {
                }
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        template: "<div class=\"page-header\">\n    <h1>Relationship  Builder</h1>\n</div> \n        <toaster-container></toaster-container>\n <template ngbModalContainer></template>\n    <div class=\"row\">\n        <div class=\"col-xs-6 col-lg-6\">\n            \n            <button routerLink=\"/\" routerLinkActive=\"active\" type=\"button\"  class=\"btn btn-primary btn-lg btn-block\">People</button>\n        </div> \n        <div class=\"col-xs-6 col-lg-6\">\n            <button routerLink=\"/tags\" routerLinkActive=\"active\" type=\"button\"  class=\"btn btn-primary btn-lg btn-block\">Tags</button>\n        </div> \n    </div>\n\n\n\n<router-outlet></router-outlet>"
                    }), 
                    __metadata('design:paramtypes', [])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});

//# sourceMappingURL=app.component.js.map
