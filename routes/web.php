<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('/tags', function () {
    return view('welcome');
});


Route::group(['prefix' => 'webapi'], function () {
    
    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'UserController@getUsers');
        Route::post('/', 'UserController@create');
        Route::get('{term}/search', 'UserController@searchUser');
        Route::post('{id_user}/relation', 'UserController@storeRelation')->where('id_user', '[0-9]+');
        Route::get('{id_user}/relation', 'UserController@getRelations')->where('id_user', '[0-9]+');
    });
    Route::group(['prefix' => 'tag'], function () {
        Route::get('/', 'TagController@getTags');
        Route::post('/', 'TagController@create');
        Route::put('/{id_tag}', 'TagController@update');
        Route::get('{term}/search', 'TagController@searchTag');
    });
});

