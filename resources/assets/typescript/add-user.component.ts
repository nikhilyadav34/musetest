import {Component, Output, EventEmitter} from '@angular/core';
import {RouterModule, Router}   from '@angular/router';
import {NgbModal, ModalDismissReasons, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {User, ApiService} from './api.service';
import {ToasterModule, ToasterService} from 'angular2-toaster/angular2-toaster';

import './rxjs-operators';
import 'rxjs/add/operator/map'
import {Observable} from 'rxjs/Observable';

@Component({
    selector: 'add-user',
    template: `
            <button type="button" class="btn btn-primary btn-group-lg" (click)="open(content)">Add User</button>

<template #content let-c="close" let-d="dismiss">
 <form  #addUserForm="ngForm" novalidate (ngSubmit)="onSubmit()"  >
  <div class="modal-header">
    <button type="button" class="close" aria-label="Close" (click)="d('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Add a new person</h4>
  </div>
  <div class="modal-body">

<div class="input-group">
  <label for="basic-url">Full Name</label>
  
  <input type="text" class="form-control"aria-describedby="basic-addon3" placeholder="Full Name"
        id="fullname" 
       required minlength="4" maxlength="100"
       [(ngModel)]="model.fullname" name="fullname" #fullname="ngModel">
       
       <div *ngIf="fullname.errors && (fullname.dirty || fullname.touched)"
     class="alert alert-danger">
    <div [hidden]="!fullname.errors.required">
      Full Name is required
    </div>
    <div [hidden]="!fullname.errors.minlength">
      Full Name  must be at least 4 characters long.
    </div>
    <div [hidden]="!fullname.errors.maxlength">
      Full Name cannot be more than 100 characters long.
    </div>
</div>
</div>
    
  </div>  
 
  <div class="modal-footer">
   <div *ngIf="submitted">Please wait...</div>
     <input type="submit" class="btn btn-primary btn-group-lg" value="Save" [disabled]="!addUserForm.form.valid" >
    <button type="button" class="btn btn-secondary" (click)="c('Close click')">Close</button>
  </div>
  </form>
</template>
  `
})
export class AddUserComponent {

    @Output() updateUsersEvent = new EventEmitter();

    closeResult: string;
    private model: User;
    private modalRef: NgbModalRef;
    private submitted: boolean = false;
    private searching:boolean=false;
    private searchFailed:boolean=false;

    constructor(private modalService: NgbModal, private router: Router, private apiService: ApiService,
                private toasterService: ToasterService) {
    }

    open(content) {
        this.modalRef = this.modalService.open(content);
        this.model = new User();
        this.router.navigate(['/']);
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }


    submitHandler(res: any) {
        if (res.status_code == 'success') {
            this.modalRef.close()
            this.toasterService.pop('success', 'Success!', 'Person has been added.');
            this.model.id_user =res.id_user;
            this.updateUsersEvent.emit(this.model);
        }else{
            this.toasterService.pop('error', 'Error!', 'An error occured. Please try again later.');
        }
        this.submitted = false;
    }

    onSubmit() {

        this.submitted = true;
        this.apiService.addUser(this.model)
            .then((res: any) => this.submitHandler(res));
    }





}