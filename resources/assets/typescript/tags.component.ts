import {Component, OnInit}  from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Tag} from './api.service';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {EditTagComponent}  from './edit-tag.component'

@Component({
  template: ` 
<div class="inline-break"></div>
 
    <div class="row">
        <div class="col-xs-6 col-lg-6 text-center">
            <add-user (updateUsersEvent)="updateUser($event)"></add-user>
        </div> 
        <div class="col-xs-6 col-lg-6 text-center">
            <add-tag (updateTagsEvent)="updateTags($event)"></add-tag>
        </div>                
    </div>
 
   <div class="inline-break"></div>
  <div class="clearfix visible-xs-block"></div>  
 
 <table class="table table-bordered">
    <tr *ngFor="let tag of tags |  orderBy : ['-id_tag']">
        <td >{{tag.label}}</td>
        <td align="right"> 
        <button class="btn btn-primary" (click)="openEdit(tag)">Edit</button>
        </td>
    </tr>
</table>`
})
export class TagsComponent {

  private tags: any;

  constructor(private route: ActivatedRoute,
              private router: Router, private modalService: NgbModal) {
  }


  ngOnInit() {
    this.route.data.forEach((data: { tags: Tag[] }) => {
      this.tags = data.tags;
    });
  }

  updateTags(tag:Tag){
    this.tags.push(tag);
  }

  refreshTags(tag:Tag){
    this.tags =this.tags.map(function (tagObject) {
      if(tagObject.id_tag==tag.id_tag){
         return tagObject=tag;
      }else{
        return tagObject
      }
    })
  }


  openEdit(tag:Tag){
    const modalRef = this.modalService.open(EditTagComponent);
    modalRef.componentInstance.tag=tag;
    modalRef.result.then((res:any)=>this.refreshTags(res))
  }



}
