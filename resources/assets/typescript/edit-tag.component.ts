import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import {RouterModule, Router}   from '@angular/router';
import {NgbModal, ModalDismissReasons, NgbModalRef, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Tag, ApiService} from './api.service';
import {ToasterModule, ToasterService} from 'angular2-toaster/angular2-toaster';

import './rxjs-operators';
import 'rxjs/add/operator/map'
import {Observable} from 'rxjs/Observable';

@Component({
    selector: 'edit-tag',
    template: `  <div class="modal-header">
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
      <h4 class="modal-title">Edit a relationship tag</h4>
    </div>
    <div class="modal-body">
      
      
      <form  #editTagForm="ngForm" novalidate (ngSubmit)="onSubmit()"  >
 
  <div class="modal-body">

<div class="input-group">
  <label for="basic-url">Relationship Tag Name</label>
 
  
  <input type="text" class="form-control"aria-describedby="basic-addon3" placeholder="Relationship Tag Name"
        id="label" 
       required minlength="4" maxlength="40"
       [(ngModel)]="model.label" name="label" #label="ngModel">
       
       <div *ngIf="label.errors && (label.dirty || label.touched)"
     class="alert alert-danger">
    <div [hidden]="!label.errors.required">
      Tag is required
    </div>
    <div [hidden]="!label.errors.minlength">
      Tag must be at least 4 characters long.
    </div>
    <div [hidden]="!label.errors.maxlength">
      Tag cannot be more than 40 characters long.
    </div>
</div>
</div>
    
  </div>  
 
  <div class="modal-footer">
   <div *ngIf="submitted">Please wait...</div>
     <input type="submit" class="btn btn-primary btn-group-lg" value="Save" [disabled]="!editTagForm.form.valid" >
    <button type="button" class="btn btn-secondary" (click)="activeModal.dismiss('Close click')">Close</button>
  </div>
  </form>
 `
})
export class EditTagComponent implements OnInit{

    @Input() tag;

    private model:Tag;
    private submitted:boolean=false;
    constructor(public activeModal: NgbActiveModal, private toasterService:ToasterService, private apiService:ApiService) {
        this.model = new Tag();
    }

    ngOnInit()    {
        this.model =Object.assign({}, this.tag);
    }



    submitHandler(res: any) {
        if (res.status_code == 'success') {
            this.activeModal.close(this.model)
            this.toasterService.pop('success', 'Success!', 'Relationship tag has been added.');
            this.model.id_tag =res.id_tag;
        }else{
            this.toasterService.pop('error', 'Error!', 'An error occured. Please try again later.');
        }
        this.submitted = false;
    }


    private handleError(error: any[]) {
        console.log(error);

    }

    onSubmit() {

        this.submitted = true;
        this.apiService.editTag(this.model)
            .then((res: any) => this.submitHandler(res))
            .catch((error:any)=>this.handleError(error));
    }

}

