import {Injectable}             from '@angular/core';
import {
    Router, Resolve,
    ActivatedRouteSnapshot
} from '@angular/router';
import {Tag, ApiService} from './api.service';

@Injectable()
export class TagsListResolve implements Resolve<Tag> {

    constructor(private apiService: ApiService, private router: Router) {
    }

    resolve(route: ActivatedRouteSnapshot): Promise<Tag>|boolean {
        return this.apiService.getTags()
            .then((tags: any) => {
                console.log(tags)
                if (tags) {
                    return tags;
                } else { // id not found
                    this.router.navigate(['/']);
                    return false;
                }
            });
    }


}
