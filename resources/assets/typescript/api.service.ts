export class UserRelations {
    constructor(public id_tag?: number,
                public label?: string,
                public id_user?: number,
                public fullname?: string,
                public status?: string,
                public created_at?: string,
                public updated_at?: string) {
    }
}

export class User {
    constructor(public id_user?: number,
                public fullname?: string,
                public status?: boolean,
                public created_at?: string,
                public updated_at?: string,
                public User_relations?: UserRelations) {
    }
}


export class Tag {
    constructor(public id_tag?: number,
                public label?: string) {
    }
}





import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {ToasterModule, ToasterService} from 'angular2-toaster/angular2-toaster';

import './rxjs-operators';
import 'rxjs/add/operator/map'
import 'rxjs/add/observable/of';
import {Observable} from 'rxjs/Observable';

@Injectable()

export class ApiService {

    private baseApi: string;

    constructor(private http: Http, private toasterService: ToasterService) {
        if(document.location.hostname=='localhost'){
            this.baseApi = '/webapi/';
        }else{
            this.baseApi = '/webapi/';
        }
        this.toasterService=toasterService;
    }


    getUsers() {
        return this.http.get(this.baseApi + 'user')
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getTags() {
        return this.http.get(this.baseApi + 'tag')
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }


    private extractData(res: Response) {
        let body = res.json();
        //console.log(body)
        return body || {};
    }

    private handleError(err: any) {
        try {
            let  error=err.json();
            if(error.status_code=='failed'){
                this.toasterService.pop('error', 'Error!', error.message);
            }else{
                this.toasterService.pop('error', 'Error!', 'An error occured. Please try again later.');
            }
            console.log(error);
        }catch ( e ){

        }
        return Promise.reject("");
    }


    addTag(tag:Tag) {
        return this.http.post(this.baseApi + 'tag', tag)
            .toPromise()
            .then(res => res.json())
            .catch((error:any)=>this.handleError(error));
    }

    editTag(tag:Tag) {
        return this.http.put(this.baseApi + `tag/${tag.id_tag}`, tag)
            .toPromise()
            .then(res => res.json())
            .catch((error:any)=>this.handleError(error));
    }

    addUser(user:User) {
        return this.http.post(this.baseApi + 'user', user)
            .toPromise()
            .then(res => res.json())
            .catch((error:any)=>this.handleError(error));
    }

    //  searchTag(tagLabel:string) {
    //     return this.http.get(this.baseApi + 'tag/${tagLabel}/search')
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }


    filterPerson(res:any, excludeKeys:any[]){
        var res = res.json();
        var filteredUser=[];
        res.forEach(function (usrObj) {
            if(excludeKeys.indexOf(usrObj.id_user)==-1){
                filteredUser.push(usrObj);
            }
        })
        return filteredUser;
    }

    searchPerson(fullName:string, excludeKeys:any[]): Observable<any> {
        if (fullName === '') {
            return Observable.of([]);
        }
        return this.http.get(this.baseApi + `user/${fullName}/search`)
            .map(res => this.filterPerson(res, excludeKeys))
            .catch(this.handleError);
    }

    searchTag(label:string): Observable<any> {
        if (label === '') {
            return Observable.of([]);
        }
        return this.http.get(this.baseApi + `tag/${label}/search`)
            .map(res => res.json())
            .catch(this.handleError);
    }


    saveRelation(id_user:number, userRel:UserRelations) {
        return this.http.post(this.baseApi + `user/${id_user}/relation`, userRel)
            .toPromise()
            .then(res => res.json())
            .catch((error:any)=>this.handleError(error));
    }


    getRelation(id_user:number) {
        return this.http.get(this.baseApi + `user/${id_user}/relation`)
            .toPromise()
            .then(res => res.json())
            .catch((error:any)=>this.handleError(error));
    }


}