import {Component, OnInit, Input, ViewChild}  from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {NgbModal, ModalDismissReasons, NgbModalRef, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {UserRelationComponent} from './user-relation.component'
import {User} from './api.service';

// import {OrderByPipe} from './orderBy'



@Component({
    // pipes: [ OrderByPipe ],
    template: `
 
<div class="inline-break"></div>
 
    <div class="row">
        <div class="col-xs-6 col-lg-6 text-center">
            <add-user (updateUsersEvent)="updateUser($event)"></add-user>
        </div> 
        <div class="col-xs-6 col-lg-6 text-center">
            <add-tag (updateTagsEvent)="updateTags($event)"></add-tag>
        </div>                
    </div>
<user-relation #myModal></user-relation>

  <div class="inline-break"></div>
  <div class="clearfix visible-xs-block"></div>  

 <table class="table table-bordered">
    <tr *ngFor="let user of users |  orderBy : ['-id_user']">
        <td >{{user.fullname}}</td>
        <td align="right"> 
        <button class="btn btn-primary" (click)="open(user)">Edit</button>
            </td>
    </tr>
</table>`
})
export class UsersComponent {

    private users: any[];
    @ViewChild(UserRelationComponent) userRelComp:UserRelationComponent;
    constructor(private route: ActivatedRoute,
                private router: Router, private modalService: NgbModal) {
    }


    ngOnInit() {
        this.route.data.forEach((data: { users: User[] }) => {
            console.log(data)
            this.users = data.users;
        });
    }


    updateUser(user:User){
        this.users.push(user);
    }


    open(user:User) {
        this.userRelComp.open(user);
    }
}
