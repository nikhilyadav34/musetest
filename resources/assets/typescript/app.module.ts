///<reference path="../../../typings/index.d.ts"/>
import {NgModule }      from '@angular/core';
import {BrowserModule } from '@angular/platform-browser';
import {RouterModule }   from '@angular/router';
import {HttpModule, JsonpModule } from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule}   from '@angular/forms';
import {ModalModule} from 'ng2-modal';
import {ToasterModule, ToasterService} from 'angular2-toaster/angular2-toaster';


import { AppComponent }   from './app.component';
import  {UsersComponent} from './users.component'
import  {AddUserComponent} from './add-user.component'
import  {AddTagComponent} from './add-tag.component'
import  {TagsComponent} from './tags.component'
import  {UsersListResolve} from './users-resolve.service'
import  {TagsListResolve} from './tags-resolve.service';
import  {ApiService} from './api.service'
import {UserRelationComponent} from './user-relation.component'
import {EditTagComponent} from './edit-tag.component'
import {OrderBy} from './orderBy'


@NgModule({
  imports:      [
    BrowserModule, FormsModule, HttpModule, JsonpModule, NgbModule.forRoot(), ToasterModule, ModalModule,
    RouterModule.forRoot([
      { path: '', component: UsersComponent,resolve: { users: UsersListResolve} },
      { path: 'tags', component: TagsComponent, resolve: { tags: TagsListResolve}},
      //{ path: '**', component: PageNotFoundComponent } 
    ])
  ],
  declarations: [ AppComponent, UsersComponent, TagsComponent, AddUserComponent, AddTagComponent,
    UserRelationComponent, UserRelationComponent, EditTagComponent, OrderBy],
  bootstrap:    [ AppComponent ],
  entryComponents: [EditTagComponent],
  providers: [UsersListResolve, TagsListResolve, ApiService]
})
export class AppModule { }
