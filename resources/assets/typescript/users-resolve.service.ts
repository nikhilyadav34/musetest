import { Injectable }             from '@angular/core';
import { Router, Resolve,
         ActivatedRouteSnapshot } from '@angular/router';
import { User, ApiService } from './api.service';

@Injectable()
export class UsersListResolve implements Resolve<User> {
    
  constructor(private apiService: ApiService, private router: Router) {}
    
  resolve(route: ActivatedRouteSnapshot): Promise<User>|boolean {
    return this.apiService.getUsers()
      .then((users:any) => {
        console.log(users)
        if (users) {
          return users;
        } else { // id not found
          this.router.navigate(['/']);
          return false;
        }
      });
  }
      
      
}
