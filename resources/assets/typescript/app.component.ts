import {Component} from '@angular/core';
@Component({
    selector: 'my-app',
    template: `<div class="page-header">
    <h1>Relationship  Builder</h1>
</div> 
        <toaster-container></toaster-container>
 <template ngbModalContainer></template>
    <div class="row">
        <div class="col-xs-6 col-lg-6">
            
            <button routerLink="/" routerLinkActive="active" type="button"  class="btn btn-primary btn-lg btn-block">People</button>
        </div> 
        <div class="col-xs-6 col-lg-6">
            <button routerLink="/tags" routerLinkActive="active" type="button"  class="btn btn-primary btn-lg btn-block">Tags</button>
        </div> 
    </div>



<router-outlet></router-outlet>`

})
export class AppComponent {
}
