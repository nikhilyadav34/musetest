import {Component, Output, Input, EventEmitter, ViewChild} from '@angular/core';
import {CommonModule}       from '@angular/common';

import {RouterModule, Router}   from '@angular/router';
import {NgbModal, ModalDismissReasons, NgbModalRef, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {User, Tag, ApiService, UserRelations} from './api.service';
import {ToasterModule, ToasterService} from 'angular2-toaster/angular2-toaster';

import './rxjs-operators';
import 'rxjs/add/operator/map'
import 'rxjs/add/observable/of';
import {Observable} from 'rxjs/Observable';


@Component({
    selector: 'user-relation',
    template: `<div class="row">
 <modal #myModal  (onOpen)="actionOnOpen()" modalClass="modal-lg">
        <modal-header>
            <h3>Add the person's relationships</h3>
        </modal-header>
        <modal-content>
        
      <div class="col-xs-6 col-sm-4">    
  <div class="row">
   <h3>{{fullname}}</h3>
  <p>Relationship</p>
</div>
</div>
   
<div class="inline-break"></div>
<div class="clearfix visible-xs-block"></div>
 
<template #rt let-r="result" let-t="term">
              {{ r.fullname}}
            </template>
            
 
<template #rtTag let-r="result" let-t="term"  >
              {{ r.label}}
            </template>           
            
 <form  #userRelationForm="ngForm" novalidate (ngSubmit)="addRelation()"   *ngIf="active">
<div class="row">
  <div class="col-xs-6 col-sm-4">
  <input type="text" name="relatatedPerson" class="form-control" aria-label="Name-Relatated Person"  
  (change)="typeHeadPersonChange($event)" [focusFirst]="true" [editable]="false" 
  required placeholder="Name-Relatated Person" #relatatedPerson="ngModel"
  [(ngModel)]="model.fullname" 
   [ngbTypeahead]="searchPerson"  [inputFormatter]="formatter" [resultTemplate]="rt"
    (selectItem)="onRelatatedSelect($event)">
    <div [hidden]="relatatedPerson.valid || relatatedPerson.pristine" class="alert alert-danger "> 
        Name-Relatated Person is required.
    </div>
       <span *ngIf="searching">searching...</span>

  </div>
  <div class="col-xs-6 col-sm-4">
  
   <input type="text" name="relatatedTag" class="form-control" aria-label="Relation Tag" 
   (change)="typeHeadTagChange($event)" [focusFirst]="true" [editable]="false" 
   required placeholder="Relation Tag" #relatatedTag="ngModel"
  [(ngModel)]="model.label" 
   [ngbTypeahead]="searchTag"  [inputFormatter]="formatterTag" [resultTemplate]="rtTag"
    (selectItem)="onRelationSelect($event)">
    <div [hidden]="relatatedTag.valid || relatatedTag.pristine" class="alert alert-danger "> 
        Relation Tag is required.
    </div>
   <span *ngIf="searching">searching...</span>
  
  
 
  <div class="inline-break"></div>
  <div class="clearfix visible-xs-block"></div>
</div>
  <div class="clearfix visible-xs-block"></div>
  <div class="col-xs-6 col-sm-4">
  <input type="submit" class="btn btn-primary" value="Add" [disabled]="!userRelationForm.form.valid" >
</div>
</div> 
       </form> 
       
      <div class="inline-break"></div>
  <div class="clearfix visible-xs-block"></div>  
  
  
  <div *ngIf="loadingRelations">Loading Relations...</div>
   <table class="table table-bordered">
    <tr *ngFor="let userRelation of userRelations">
        <td>{{fullname}}</td>
        <td>{{userRelation.label}}</td>
        <td>{{userRelation.fullname}}</td>
        <td>{{userRelation.degree}}</td>
         
    </tr>
</table>     
          
          
          
        </modal-content>
        <modal-footer>
            <button class="btn btn-primary" (click)="myModal.close()">close</button>
        </modal-footer>
    </modal>
</div>
`
})
export class UserRelationComponent {
    private model: UserRelations;
    @ViewChild('myModal') myModal;
    private searching: boolean = false;
    private searchFailed: boolean = false;
    private currentUser: any;
    private fullname: string;
    private currentRelatatedUser: User;
    private currentRelationTag: Tag;
    private userRelations: UserRelations[];
    private active: boolean = true;
    private excludeKeys: number[];
    private loadingRelations: boolean = false;

    constructor(private apiService: ApiService, private toasterService: ToasterService) {
        this.model = new UserRelations();
    }


    private handleError(error: any[]) {
        console.log(error);
        return Observable.of([]);
    }

    open(currentUser: any) {
        this.loadingRelations = true;
        this.currentUser = currentUser;
        this.fullname = currentUser.fullname;

        this.apiService.getRelation(currentUser.id_user)
            .then((userRelations: any)=> {
                this.userRelations = userRelations;
                this.loadingRelations = false;
                this.getExcludeKeys();
            })
            .catch((error: any)=> {
                this.toasterService.pop('error', 'Error!', 'An error occured. Please try again later.');
                this.loadingRelations = false;
            });

        // this.userRelations = currentUser.user_relations;
        this.myModal.open()
        this.model = new UserRelations();

    }


    typeHeadPersonChange(eventData: any) {
        this.model.fullname = '';
    }

    typeHeadTagChange(eventData: any) {
        this.model.label = '';
    }


    actionOnOpen() {
        this.model = new UserRelations();
    }

    onRelatatedSelect(event: any) {
        this.currentRelatatedUser = event.item;

    }


    getExcludeKeys() {
        let excludeKeys = [];
        this.userRelations.forEach(function (usrObj) {
            excludeKeys.push(usrObj.id_user);
        })
        this.excludeKeys = excludeKeys;
        this.excludeKeys.push(this.currentUser.id_user);
        // console.log(this.excludeKeys)
    }


    onRelationSelect(event: any) {
        this.currentRelationTag = event.item;
    }

    onRelationAdd(res: any, user_relations: any) {
        // console.log(this.currentUser)
        if (res.status_code == 'success') {
            this.userRelations=[];
            this.apiService.getRelation(this.currentUser.id_user)
                .then((userRelations: any)=> {
                    this.userRelations = userRelations;
                    this.loadingRelations = false;
                    this.getExcludeKeys();
                })
                .catch((error: any)=> {
                    this.loadingRelations = false;
                });

            this.model = new UserRelations();
            this.active = false;
            this.getExcludeKeys();
            setTimeout(() => this.active = true, 0);
            this.toasterService.pop('success', 'Success!', 'Relationship has been updated.');
        } else {
            this.toasterService.pop('error', 'Error!', 'An error occured. Please try again later.');
        }

    }

    onRelationError(err: any) {
        // this.toasterService.pop('error', 'Error!', 'An error occured. Please try again later.')
        console.log(err)
    }

    addRelation() {
        let user_relations: any = this.currentRelatatedUser;
        user_relations.id_tag = this.currentRelationTag.id_tag;
        user_relations.label = this.currentRelationTag.label;

        this.apiService.saveRelation(this.currentUser.id_user, user_relations)
            .then((res: any)=>this.onRelationAdd(res, user_relations))
            .catch((err: any)=>this.onRelationError(err))

    }

    searchPerson = (text$: Observable<string>) =>
        text$
            .debounceTime(300)
            .distinctUntilChanged()
            .do(() => this.searching = true)
            .switchMap(term =>
                this.apiService.searchPerson(term, this.excludeKeys)
                    .do(() => this.searchFailed = false)
                    .catch(this.handleError)
            )
            .do(() => this.searching = false);
    formatter = (x: {fullname: string}) => x.fullname;


    searchTag = (text$: Observable<string>) =>
        text$
            .debounceTime(300)
            .distinctUntilChanged()
            .do(() => this.searching = true)
            .switchMap(term =>
                this.apiService.searchTag(term)
                    .do(() => this.searchFailed = false)
                    .catch(this.handleError)
            )
            .do(() => this.searching = false);

    formatterTag = (xTag: {label: string}) => xTag.label;


}