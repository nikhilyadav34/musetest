<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <base href="/">
        <!-- 1. Load libraries -->
        <!-- Polyfill(s) for older browsers -->
        <script src="{{ asset('/') }}core-js/client/shim.min.js"></script>
        <script src="{{ asset('/') }}zone.js/dist/zone.js"></script>
        <script src="{{ asset('/') }}reflect-metadata/Reflect.js"></script>
        <script src="{{ asset('/') }}systemjs/dist/system.src.js"></script>
        <script src="{{ asset('/') }}systemjs.config.js"></script> 
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script> 
        <link href="{{ asset('/') }}css/app.css" rel="stylesheet">
        <link href="{{ asset('/') }}css/stickyFooter.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="angular2-toaster/lib/toaster.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script>
            System.import('app').catch(function (err) {
                console.error(err);
            });
        </script>

    </head>
    <body>
        <div id="wrap"> 
            <div class="container">
                
               
                
                <my-app>Loading...</my-app>
            </div>
        </div>
        <div id="footer">
            <div class="container">
                <p class="muted">This site is created for skill demonstration purpose, It's code cannot be used for
                                                    production version without the permission of developer.</p>
            </div>
        </div>
    </body>

</html>