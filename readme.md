# User Relations 

##Requirements:
* PHP >= 5.6.4
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
* Nodejs>  v6.0.0
* npm
* Angular 2.0 
* composer must be installed on your system 
* gulp should be installed globally



1)  Clone repos
```cmd
git clone https://nikhilyadav34@bitbucket.org/nikhilyadav34/musetest.git
```
2) composer install in root directory of project
```cmd
composer install
```
3) npm install in root directory
```cmd
npm install
```

4) run gulp if you made any changes in typescript
```cmd
gulp
```

5) Configure database credentials in .env file located in root.
6) run php artisan serve
```cmd
php artisan serve
```

If there is any key-gen issue in Laravel. you can generate key by 
```cmd
php artisan key:generate
```


#Degree of Separation:
* Degree separation mysql query between two people is 1
* Concept : Taken from 'Frigyes Karinthy' Six degrees of separation theory 
* Link :https://en.wikipedia.org/wiki/Six_degrees_of_separation
* 1) First Degree of separation friend of friend (query 1)
* 2) Second Degree of separation friend of friend of friend  (query 2)
* 3) Third degree, fourth degree so on upto  4  degree
* because 5-6 degree will start and end person.
* So we are calculation 6 level degree of separation in 2 people 
* 7) Calculation
